<?php

use oxusmedia\webApp\controller;
use oxusmedia\webApp\MyWebauthn;
use oxusmedia\webApp\form;
use oxusmedia\webApp\column;
use oxusmedia\webApp\input;
use oxusmedia\webApp\password;
use oxusmedia\webApp\button;

class site extends controller
{
    const LOGIN_METHOD_PASSWORD = 'P';
    const LOGIN_METHOD_WEBAUTHN = 'W';

    public function index()
    {
        $this->webApp()->requireLoginRedir();

        $this->titulo = 'Bienvenido';

        $this->render('index');
    }

    public function login()
    {
        if ($this->webApp()->isLoggedIn())

            $this->redirect($this->webApp()->getSite());

        else {

            $form = new form('login', array(
                new column(array(
                    ($this->webApp()->getConfig('LOGIN_WITH_EMAIL')
                        ? new input('email', array(
                            'rules'       => array(
                                'required' => true,
                                'email'    => true
                            ),
                            'htmlOptions' => array(
                                'placeholder' => 'Email',
                                'class'       => 'step1'
                            ),
                            'value'       => $_COOKIE['login_email'] ?? ''
                        ))
                        : new input('usuario', array(
                            'rules'       => array(
                                'required' => true
                            ),
                            'htmlOptions' => array(
                                'placeholder' => 'Usuario',
                                'class'       => 'step1'
                            )
                        ))
                    ),
                    new password('pass', array(
                        'htmlOptions' => array(
                            'placeholder' => 'Contraseña',
                            'class'       => 'step2'
                        )
                    ))
                ))
            ), array(
                'buttons' => array(
                    new button('siguiente', button::SUBMIT, button::PRIMARY),
                    new button('cancelar', button::BUTTON, button::SECONDARY, array(
                        'htmlOptions' => array(
                            'class' => 'step2'
                        )
                    ))
                )
            ));

            if (isset($_POST['login'])) {

                $form->setAtributes($_POST['login']);

                if ($form->validate()) {

                    $param = $form->getAtributes();

                    if ($this->webApp()->login($param['email'], $param['pass'])) {

                        $this->webApp()->setCookie('login_email', $param['email']);
                        $this->webApp()->setCookie('login_method', $this::LOGIN_METHOD_PASSWORD);

                        $this->redirect($this->getMethodUrl('registerWebauthn'));

                        return;

                    }

                }

            }

            $this->titulo = 'Iniciar sesión';

            MyWebauthn::initialize();

            $this->addCss($this->webApp()->getUrlAssets() . 'css/login.css');

            $this->addJs($this->webApp()->getUrlAssets() . 'webapp/js/jquery.validate.min.js');
            $this->addJs($this->webApp()->getUrlAssets() . 'webapp/js/additional-methods.min.js');
            $this->addJs($this->webApp()->getUrlAssets() . 'webapp/js/jquery.validate.messages_es_AR.js');

            $this->render('login', array(
                'method' => $_COOKIE['login_method'] ?? $this::LOGIN_METHOD_PASSWORD,
                'form'   => $form
            ));

        }

    }

    public function loginWebauthnStep1()
    {
        if (isset($_GET['email']))
            $this->returnJson(
                MyWebauthn::loginStep1($_GET['email'])
            );
    }

    public function loginWebauthnStep2()
    {
        $post = trim(file_get_contents('php://input'));

        if ($post and isset($_GET['email'])) {

            $return = MyWebauthn::loginStep2($post, $_GET['email']);

            if ($return->success) {
                $this->webApp()->setCookie('login_email', $_GET['email']);
                $this->webApp()->setCookie('login_method', $this::LOGIN_METHOD_WEBAUTHN);
            }

            $this->returnJson($return);

        }

    }

    public function registerWebauthn()
    {
        $this->titulo = 'Bienvenido';

        MyWebauthn::initialize();

        $this->addJs($this->webApp()->getUrlAssets() . 'webapp/js/jquery.validate.min.js');
        $this->addJs($this->webApp()->getUrlAssets() . 'webapp/js/additional-methods.min.js');
        $this->addJs($this->webApp()->getUrlAssets() . 'webapp/js/jquery.validate.messages_es_AR.js');

        $form = new form('register', array(
            new column(array(
                new input('dispositivo', array(
                    'label'       => '',
                    'rules'       => array(
                        'required' => true
                    ),
                    'htmlOptions' => array(
                        'placeholder' => 'Dispositivo'
                    )
                ))
            ))
        ), array(
            'buttons' => array(
                new button('siguiente', button::SUBMIT, button::PRIMARY),
                new button('cancelar', button::BUTTON, button::SECONDARY)
            )
        ));

        $this->render('webauthn', array(
            'form' => $form
        ));
    }

    public function registerWebauthnStep1()
    {
        $this->webApp()->requireLogin();

        $this->returnJson(
            MyWebauthn::registerStep1($this->webApp()->getUsuarioId())
        );
    }

    public function registerWebauthnStep2()
    {
        $this->webApp()->requireLogin();

        $post = trim(file_get_contents('php://input'));

        if ($post) {

            $return = MyWebauthn::registerStep2($post, $_GET['device']);

            if ($return->success)
                $this->webApp()->setCookie('login_method', $this::LOGIN_METHOD_WEBAUTHN);

            $this->returnJson($return);

        }
    }

    public function logout()
    {
        $this->webApp()->logout();

        $this->redirect($this->getMethodUrl('login'));
    }

    public function downloadCertificates()
    {
        $return = MyWebauthn::downloadFidoCertificates();

        $this->returnJson($return);
    }

}