<?php $this->renderInclude("header");?>

    <hr class="page-title-hr">

    <div class="step step1">

        <h3>¿Desea habilitar el inicio de sesión con <span id="os"></span> en este dispositivo?</h3>

        <div class="acciones">
            <button type="button" class="btn btn-primary" id="enable">Habilitar</button>
            <button type="button" class="btn btn-secondary" id="cancel">Cancelar</button>
        </div>

    </div>

    <div class="step step2">

        <h3>Ingrese un nombre para identificar este dispositivo</h3>

        <?php echo $form->render();?>

    </div>

    <script>

        $(document).ready(function(){

            if (is_webauthn_supported()) {

                $('#os').html(webauthn_os());

                let step;
                step1();

                $('#enable').click(function(e) {
                    step2();
                    e.preventDefault();
                });

                $('#register').submit(function(e) {
                    if ($(this).valid())
                        webauthn_register($('#dispositivo').val());
                    e.preventDefault();
                });

                $('#cancel').click(function(e) {
                    window.location = '';
                    e.preventDefault();
                });

                $('#cancelar').click(function(e) {
                    window.location = '';
                    e.preventDefault();
                });

                function step1() {
                    $('.step1').show();
                    $('.step2').hide();
                    step = 1;
                }

                function step2() {
                    $('.step1').hide();
                    $('.step2').show();
                    step = 2;
                }

            } else {

                window.location = '';

            }

        });

    </script>

<?php $this->renderInclude("footer");?>
