<!DOCTYPE html>
<html lang="es">
<head>

    <title><?php echo $this->titulo . ' | ' . $this->webApp()->getConfig('TITULO'); ?></title>

    <?php $this->renderInclude("head"); ?>

    <script>

        $(document).ready(function(){

            let step;
            step1();

            $('#cancelar').click(function(e){
                step1();
                e.preventDefault();
            });

            $('#login').submit(function(e){
                if ($(this).valid()) {
                    if (step === 1) {
                        <?php if ($method == $this::LOGIN_METHOD_WEBAUTHN) { ?>
                            $('#hello').click();
                        <?php } else { ?>
                            step2();
                        <?php } ?>
                        e.preventDefault();
                    }
                }
            });

            $('#hello').click(function(e) {
                webauthn_login($('#email').val(), function() {
                    step2();
                });
                e.preventDefault();
            });

            function step1()
            {
                $('.step1').show();
                $('.step2').hide();
                $('#siguiente').html('Siguiente');
                $('#email').focus();
                step = 1;
            }

            function step2()
            {
                $('.step1').hide();
                $('.step2').show();
                $('#siguiente').html('Inciar sesión');
                if (is_webauthn_supported()) {
                    $('#os').html(webauthn_os());
                } else {
                    $('.hello').hide();
                }
                $('#pass').focus();
                step = 2;
            }

        });

    </script>

</head>
<body>

    <div class="login-wrap">

        <h2 class="logo">Iniciar sesión</h2>

        <?php echo $form->render();?>

        <p class="hello step2">
            <a href="#" id="hello">Iniciar sesión con <span id="os"></span></a>
        </p>

        <?php /*

        <form role="form" id="login" method="post">

            <div class="step step1">
                <div class="form-group">
                    <input type="email" name="email" id="email" value="<?php echo $email;?>" class="form-control" placeholder="Email" required>
                </div>
                <div class="acciones">
                    <p>
                        <button type="submit" class="btn btn-primary">Siguiente</button>
                    </p>
                </div>
            </div>

            <div class="step step2">
                <div class="form-group">
                    <input type="password" name="pass" id="pass" value="" class="form-control" placeholder="Contraseña">
                </div>
                <div class="acciones">
                    <p>
                        <button type="submit" id="submit" class="btn btn-primary">Iniciar Sesión</button>
                    </p>
                    <p>
                        <button type="button" id="cancel" class="btn btn-secondary">Cancelar</button>
                    </p>
                    <p class="hello">
                        <a href="#" id="hello">Iniciar sesión con <span id="os"></span></a>
                    </p>
                </div>
            </div>

        </form>

        */ ?>

    </div>

</body>
</html>