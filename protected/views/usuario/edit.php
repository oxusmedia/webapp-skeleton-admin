
<?php echo $form->render();?>

<?php if ($this->db()->numRows($dispositivos) > 0) { ?>

    <hr>

    <h3>Dispositivos</h3>

    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>Dispositivo</th>
            <th>Registrado</th>
            <th>Último inicio de sesión</th>
        </tr>
        </thead>
        <tbody>
        <?php while ($d = $this->db()->getRow($dispositivos)) { ?>
            <tr>
                <td><?php echo $d->dispositivo;?></td>
                <td><?php echo $d->registrado;?></td>
                <td><?php echo $d->ultimo_login;?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

<?php } ?>
