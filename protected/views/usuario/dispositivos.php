
<?php $this->renderInclude("header");?>

    <hr class=".page-title-hr">

    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Dispositivo</th>
                <th>Registrado</th>
                <th>Último inicio de sesión</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php while ($d = $this->db()->getRow($dispositivos)) { ?>
                <tr>
                    <td><?php echo $d->dispositivo;?></td>
                    <td><?php echo $d->registrado;?></td>
                    <td><?php echo $d->ultimo_login;?></td>
                    <td><button type="button" class="btn btn-danger btn-xs delete" data-id="<?php echo $d->id;?>">Eliminar</button></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <script>

        $(document).ready(function(){
            $('.delete').click(function(){
                const id = $(this).attr('data-id');
                $.webApp_modal('¿Desea eliminar el dispositivo?', 'Eliminar', [
                    {
                        text: 'Eliminar',
                        eventFunction: function(){
                            $.blockUI();
                            window.location = 'usuario/dispositivos?id=' + id;
                        }
                    },
                    {
                        text: 'Cancelar',
                        close: true
                    }
                ], 'S');
            });
        });

    </script>

<?php $this->renderInclude("footer");?>
