$(function() {

    $('#side-menu').metisMenu();

    $("#contract").click(function(e) {
        e.preventDefault();
        var w = $("#wrapper");
        if (w.hasClass("contracted")) {
            w.removeClass("contracted");
            setCookie('sidebar-contracted', '0');
        } else {
            w.addClass("contracted");
            setCookie('sidebar-contracted', '1');
        }
        setLinkContent();
        $(window).trigger('resize');
    });

    setLinkContent();

    function setLinkContent() {
        if ($("#wrapper").hasClass("contracted"))
            $("#contract").html('<i class="fa fa-arrow-circle-o-right fa-fw"></i> <span>Cerrar menu</span>');
        else
            $("#contract").html('<i class="fa fa-arrow-circle-o-left fa-fw"></i> <span>Cerrar menu</span>');
    }

    function setCookie(name, value, path) {
        if (path == null) {
            var arr = _webApp_site.replace('http://', '').replace('https://', '').split('/');
            arr.splice(0);
            path = '/' + arr.join('/') + (arr.length > 0 ? '/' : '');
        }
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        document.cookie = name + "=" + value + "; expires=" + d.toUTCString() + "; path=/";
    }

    if ($.blockUI) {
        $.blockUI.defaults.message = '<img src="assets/images/ajax-loader.svg" alt="">';
        $.blockUI.defaults.css = {
            padding  : 0,
            margin   : '0 auto',
            width    : '30%',
            top      : '40%',
            left     : '35%',
            textAlign: 'center',
            color    : '#000'
        };
        $.blockUI.defaults.overlayCSS = {
            backgroundColor: '#000',
            opacity        : 0.8
        }
    }
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        let width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768)
            $('div.navbar-collapse').addClass('collapse');
        else
            $('div.navbar-collapse').removeClass('collapse');
    });

    const url = window.location;
    const element = $('ul.nav a').filter(function() {
        return this.href == url /*|| url.href.indexOf(this.href) == 0*/;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});
